import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;

import java.io.File;
import java.util.*;

/**
 * Created by Mitra on 2/3/2016.
 */
public class OntologyOwlAPI {
    static OWLOntologyManager man;
    static OWLDataFactory fact;
    public static List<List<String>> allOntologyPhrases = new ArrayList<List<String>>();
    static String unvisited = "unvisited" ;
    static String visited = "visited";
    static String visiting = "visiting" ;
    public static ArrayList<ArrayList<String>> relations = new ArrayList<>();
    static List<AddAxiom> myList = new ArrayList<AddAxiom>();
    public static HashMap<String, String> map = new HashMap<String, String>();
    public static OWLOntology ontology;
    /**
     * loads the ontology from the specified file
     * @param ontologyFile
     */
    public static OWLOntology loader(File ontologyFile){
        try {
            man = OWLManager.createOWLOntologyManager();
            fact = man.getOWLDataFactory();
            //File file  = new File(ontologyFile);
            IRI ontologyIRI = IRI.create(ontologyFile);
            ontology = man.loadOntology(ontologyIRI);
        }catch (OWLException e) {
            e.printStackTrace();
        }
        return ontology;
    }
    /**
     * returns all the phrases in the ontology
     * @param ontology
     * @return
     */
    public static List<List<String>> ListOfOntologyPhrases(OWLOntology ontology){
        for (OWLClass cls : ontology.getClassesInSignature()){
            List sublist = Arrays.asList(cls.getIRI().toString().substring(cls.getIRI().toString().indexOf('#')+1).toLowerCase().split("_"));
            allOntologyPhrases.add(sublist);
        }
        return allOntologyPhrases;
    }
    /**
     * checks the if the class exists in the ontology or not
     * @param className
     * @param ontology
     * @return
     */
    public static boolean classDoesExists(String className, OWLOntology ontology){
        for (OWLClass cls : ontology.getClassesInSignature()){
            if (className.compareToIgnoreCase(cls.getIRI().toString().substring(cls.getIRI().toString().indexOf('#')+1))==0){
                return true;
            }
        }
        return false;
    }
    /**
     * checks the subClass relationship between two phrase in ontology
     * @param child
     * @param parent
     * @param ontology
     */
    /**
     * checks the subClass relationship between two phrase in ontology
     * @param child
     * @param parent
     * @param ontology
     */
    public static boolean isSubclassof(String child, String parent, OWLOntology ontology){
        for (OWLClass cls : ontology.getClassesInSignature()){
            if (parent.compareToIgnoreCase(cls.getIRI().toString().substring(cls.getIRI().toString().indexOf('#')+1))==0){
                for (OWLClassExpression children : cls.getSubClasses(ontology)){
                    if (child.compareToIgnoreCase(children.asOWLClass().getIRI().toString().substring(children.asOWLClass().getIRI().toString().indexOf('#')+1))==0){
                        return true;
                    }
                }
            }
        }
        return false;
    }
    /**
     * check if two classes are equivalent in ontology.
     * @param ancestor
     * @param child
     * @param ontology
     */
    public static boolean isEquivalent(String ancestor,String child, OWLOntology ontology){
        if (isSubclassof(ancestor, child, ontology)){
            if(isSubclassof(child,ancestor, ontology)){
                return true;
            }
        }
        for(OWLClass cls : ontology.getClassesInSignature()){
            if (ancestor.compareToIgnoreCase(cls.getIRI().toString().substring(cls.getIRI().toString().indexOf('#')+1))==0){
                for(OWLClassExpression synonyms : cls.getEquivalentClasses(ontology)){
                    if (child.compareToIgnoreCase(synonyms.asOWLClass().getIRI().toString().substring(synonyms.asOWLClass().getIRI().toString().indexOf('#')+1))==0){
                        return true;
                    }
                }
            }
        }
        return false;
    }
    /**
     * finds the list of parents for a class in ontology.
     * @param className
     * @param ontology
     */
    public static List<String> getParentList(String className, OWLOntology ontology){
        List<String> parentList =  new ArrayList<String>();
        for (OWLClass cls : ontology.getClassesInSignature()){
            if (className.compareToIgnoreCase(cls.getIRI().toString().substring(cls.getIRI().toString().indexOf('#')+1))==0){
                //We found the class
                for (OWLClassExpression parent : cls.getSuperClasses(ontology)){
                    if(!parentList.contains(parent.asOWLClass().getIRI().toString().substring(parent.asOWLClass().getIRI().toString().indexOf('#')+1))){
                        parentList.add(parent.asOWLClass().getIRI().toString().substring(parent.asOWLClass().getIRI().toString().indexOf('#')+1));
                    }
                }
            }
        }
        return parentList;
    }
    public static List<String> getEquivalentList(String className, OWLOntology ontology){
        List<String> equivalentList =  new ArrayList<String>();
        for (OWLClass cls : ontology.getClassesInSignature()){
            if (className.compareToIgnoreCase(cls.getIRI().toString().substring(cls.getIRI().toString().indexOf('#')+1))==0){
                //We found the class
                if (!cls.getEquivalentClasses(ontology).isEmpty()) {
                    for (OWLClassExpression equiv : cls.getEquivalentClasses(ontology)) {
                        if(!equivalentList.contains(equiv.asOWLClass().getIRI().toString().substring(equiv.asOWLClass().getIRI().toString().indexOf('#')+1)))
                            equivalentList.add(equiv.asOWLClass().getIRI().toString().substring(equiv.asOWLClass().getIRI().toString().indexOf('#')+1));
                    }
                }
            }
        }
        return equivalentList;
    }
    /**
     * checks if two class(phrase) has direct or indirect relationship in ontology based on DFS
     * @param ancestor
     * @param child
     * @param ontology
     */
    public static boolean isAncestorOf(String ancestor, String child, OWLOntology ontology) {
        if(isSubclassof(child,ancestor,ontology)){
            return true;
        }
        if(isEquivalent(ancestor, child, ontology)){
            return false;
        }
        LinkedList<String> q = new LinkedList<String>();
        for (OWLClass u : ontology.getClassesInSignature()) { // method defined in Graph class. not in java APIs
            // set all states as unvisited
            map.put(u.getIRI().toString().toLowerCase().substring(u.getIRI().toString().indexOf('#')+1), unvisited);

        }
	    /*
	     * start.state = State.Visiting; // start was passed in
           q.add(start); //add start onto stack.
	     */
        if(map.containsKey(child.toLowerCase())){
            map.put(child.toLowerCase(), visiting);
            q.add(child);
            String curr;
            while(!q.isEmpty()) { // while q is not empty
                curr = q.removeFirst(); // i.e., pop(), LinkedList.removeFirst() is in java API
                if (curr != null) { // if there is still something on q
                    OWLClass currClass = fact.getOWLClass(IRI.create("#" + curr ));
                    List<String> parentList = getParentList(curr, ontology);
                    for(String parent : parentList)// for each node v adjacent t
                    {
                        if(map.get(parent.toLowerCase()).contentEquals(unvisited)){
                            if (parent.equalsIgnoreCase(ancestor)){
                                return true; // there is a path
                            }
                            else {
                                map.put(parent.toLowerCase(), visiting);
                                q.add(parent.toLowerCase());// add each adjacent node current parent to q
                            }
                        }
                    }
                }
                map.put(curr.toLowerCase(), visited);
            }
        }
        return false;
    }
    public static boolean getAncestorOf( String child, OWLOntology ontology) {
        LinkedList<String> q = new LinkedList<String>();
        for (OWLClass u : ontology.getClassesInSignature()) { // method defined in Graph class. not in java APIs
            // set all states as unvisited
            map.put(u.getIRI().toString().substring(u.getIRI().toString().indexOf('#')+1), unvisited);
        }
	    /*
	     * start.state = State.Visiting; // start was passed in
           q.add(start); //add start onto stack.
	     */
        if(map.containsKey(child)){
            map.put(child, visiting);
            q.add(child);
            String curr;
            while(!q.isEmpty()) { // while q is not empty
                curr = q.removeFirst(); // i.e., pop(), LinkedList.removeFirst() is in java API
                if (curr != null) { // if there is still something on q
                    OWLClass currClass = fact.getOWLClass(IRI.create("#" + curr ));
                    List<String> parentList = getParentList(curr, ontology);
                    List<String> equivalentList = getEquivalentList(curr,ontology);
                    List<String> adjacentNodes = new ArrayList<String>();
                    adjacentNodes.addAll(parentList);
                    adjacentNodes.addAll(equivalentList);
                    for(String adjacentNode : adjacentNodes)// for each node v adjacent t
                    {
                        if(map.get(adjacentNode).contentEquals(unvisited)){
                            map.put(adjacentNode, visiting);
                            q.add(adjacentNode);// add each adjacent node current parent to q
                        }
                    }
                }
                map.put(curr, visited);
            }
        }
        return true;
    }
}

