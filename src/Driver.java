/**
 * Created by Mitra on 2/3/2016.
 */
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.semanticweb.owlapi.model.OWLOntology;

import java.io.*;
import java.util.*;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Driver {
    // public static List ancestors = new ArrayList();
    // public static List equivalents = new ArrayList<>();
    public static Map<Integer, Object[]> data = new TreeMap<Integer, Object[]>();
   public static List<String> distinctOntologyPhrases = new ArrayList<>();
    public static void main(String args[]) {
        File ontologyFile = new File(args[0]);
        OWLOntology ontology = OntologyOwlAPI.loader(ontologyFile);
        try {

            FileInputStream file = new FileInputStream(new File("mappings.xlsx"));
            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);
            Sheet firstSheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = firstSheet.iterator();
            while (iterator.hasNext()) {
                Row nextRow = iterator.next();
                int rowIndex = nextRow.getRowNum();
                if (rowIndex == 0) {
                    nextRow = iterator.next();
                    rowIndex ++;
                }
                List ancestors = new ArrayList();
                List equivalents = new ArrayList<>();
                Iterator<Cell> cellIterator = nextRow.cellIterator();
                while (cellIterator.hasNext()) {
                    Cell nextCell = cellIterator.next();
                    int columnIndex = nextCell.getColumnIndex();
                    if (columnIndex == 3) {

                        String value = nextCell.getStringCellValue();
                        if (!value.isEmpty()) {
                            List<String> ontologyPhrases = Arrays.asList(value.split(", "));
                            for (String ontologyPhrase : ontologyPhrases) {
                                if (!distinctOntologyPhrases.contains(ontologyPhrase)){
                                    distinctOntologyPhrases.add(ontologyPhrase);
                                }
                                System.out.println("\n======================================================================");
                                System.out.println("ontologyPhrase: " + ontologyPhrase);
                                List<String> temp = new ArrayList<>();
                                temp = OntologyOwlAPI.getEquivalentList(ontologyPhrase, ontology);
                                if (!temp.isEmpty()) {
                                    equivalents.addAll(temp);
                                    System.out.println("These phrases are equivalent to " + ontologyPhrase);
                                    for (Object equiv : equivalents) {
                                        System.out.print(equiv.toString() + ", ");
                                    }
                                }
                                OntologyOwlAPI.getAncestorOf(ontologyPhrase, ontology);
                                Iterator it = OntologyOwlAPI.map.entrySet().iterator();
                                while (it.hasNext()) {
                                    Map.Entry pair = (Map.Entry) it.next();
                                    if (pair.getValue().equals(OntologyOwlAPI.visited)) {
                                        if (!pair.getKey().toString().equalsIgnoreCase(ontologyPhrase) && !ancestors.contains(pair.getKey().toString()) && !pair.getKey().toString().equalsIgnoreCase("Thing")) {
                                            ancestors.add(pair.getKey());
                                        }
                                    }
                                    it.remove(); // avoids a ConcurrentModificationException
                                }
                                System.out.println("The ancestors of " + ontologyPhrase);
                                for (Object anc : ancestors) {
                                    System.out.print(anc.toString() + ", ");
                                }
                            }
                        }
                    }
                }
                String eqvString = "";
                if (!equivalents.isEmpty()) {
                    //Iterator iterator1 = equivalents.iterator();
                    System.out.println("Equivalent size: " +equivalents.size() );
                    int i ;
                    for (i=0 ; i < equivalents.size() ; i++){
                        if (i == 0){
                            eqvString += equivalents.get(i);
                        }
                        else {
                            eqvString += ", "+ equivalents.get(i);
                        }
                    }
                }
                String ancesString = "";
                if(!ancestors.isEmpty()) {
                    Iterator i = ancestors.iterator();
                    if (i.hasNext()) {
                        ancesString += i.next().toString();
                        while (i.hasNext())
                            ancesString += ", " + i.next().toString();
                    }
                    System.out.println("AncestorList : " + ancesString);
                }
                data.put(rowIndex, new Object[]{eqvString, ancesString});
                //writeToExcelFile(rowIndex, equivalents, ancestors);
            }
            file.close();
            writeToExcelFile();
            System.out.println("List of phrases mapped to ontology:");
            for(String ph : distinctOntologyPhrases){
                System.out.println(ph);
            }
            System.out.println(distinctOntologyPhrases.size());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public static void writeToExcelFile(){
        XSSFWorkbook workbook = new XSSFWorkbook();

        //Create a blank sheet
        XSSFSheet sheet0 = workbook.createSheet("new sheet");
        //Iterate over data and write to sheet
        Set<Integer> keyset = data.keySet();
        int rownum = 0;
        for(Integer key : keyset){
            Row row = sheet0.createRow(rownum++);
            Object [] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr)
            {
                Cell cell = row.createCell(cellnum++);
                cell.setCellValue((String)obj);
            }
        }
            try {
                FileOutputStream outFile = new FileOutputStream(new File("output.xlsx"));
                workbook.write(outFile);
                outFile.close();
                System.out.println(" written successfully to disk.");

            } catch (Exception e) {
                e.printStackTrace();
            }
    }
}
